using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Kiran.Models;

namespace D08Kiran.Controllers
{
    [Produces("application/json")]
    [Route("api/Bikes")]
    public class BikesController : Controller
    {
        private AppDbContext _context;

        public BikesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Bikes
        [HttpGet]
        public IEnumerable<Bike> GetBikes()
        {
            return _context.Bikes;
        }

        // GET: api/Bikes/5
        [HttpGet("{id}", Name = "GetBike")]
        public IActionResult GetBike([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Bike bike = _context.Bikes.Single(m => m.Id == id);

            if (bike == null)
            {
                return HttpNotFound();
            }

            return Ok(bike);
        }

        // PUT: api/Bikes/5
        [HttpPut("{id}")]
        public IActionResult PutBike(int id, [FromBody] Bike bike)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != bike.Id)
            {
                return HttpBadRequest();
            }

            _context.Entry(bike).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BikeExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Bikes
        [HttpPost]
        public IActionResult PostBike([FromBody] Bike bike)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Bikes.Add(bike);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (BikeExists(bike.Id))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetBike", new { id = bike.Id }, bike);
        }

        // DELETE: api/Bikes/5
        [HttpDelete("{id}")]
        public IActionResult DeleteBike(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Bike bike = _context.Bikes.Single(m => m.Id == id);
            if (bike == null)
            {
                return HttpNotFound();
            }

            _context.Bikes.Remove(bike);
            _context.SaveChanges();

            return Ok(bike);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BikeExists(int id)
        {
            return _context.Bikes.Count(e => e.Id == id) > 0;
        }
    }
}
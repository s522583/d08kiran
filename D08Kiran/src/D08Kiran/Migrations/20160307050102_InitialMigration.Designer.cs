using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08Kiran.Models;

namespace D08Kiran.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160307050102_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08Kiran.Models.Bike", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Color");

                    b.Property<string>("EngineType");

                    b.Property<string>("GearType");

                    b.Property<int?>("LocationID");

                    b.Property<string>("Model");

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("D08Kiran.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<int?>("BikeId");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08Kiran.Models.Bike", b =>
                {
                    b.HasOne("D08Kiran.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });

            modelBuilder.Entity("D08Kiran.Models.Location", b =>
                {
                    b.HasOne("D08Kiran.Models.Bike")
                        .WithMany()
                        .HasForeignKey("BikeId");
                });
        }
    }
}

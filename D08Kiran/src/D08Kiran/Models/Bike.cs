﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace D08Kiran.Models
{
    public class Bike
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }


        [Display(Name = "Name")]
        public string Name { get; set; }


        [Display(Name = "Model")]
        public string Model { get; set; }


        [Display(Name = "Color")]
        public String Color { get; set; }


        [Display(Name = "GearType")]
        public String GearType { get; set; }

        [Display(Name = "EngineType")]
        public String EngineType { get; set; }


        [ScaffoldColumn(true)]
        public Int32? LocationID { get; set; }


        public virtual Location Location { get; set; }

        public List<Location> dealerLocation { get; set; }

        public static List<Bike> ReadAllFromCSV(string filepath)
        {
            List<Bike> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Bike.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Bike OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Bike item = new Bike();

            int i = 0;
            item.Name = Convert.ToString(values[i++]);
            item.Model = Convert.ToString(values[i++]);
            item.Color = Convert.ToString(values[i++]);
            item.GearType = Convert.ToString(values[i++]);
            item.EngineType = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }

    }
}

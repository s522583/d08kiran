﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace D08Kiran.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet<Bike> Bikes { get; set; }
        public DbSet<Location> Locations { get; set; }  
    }
}
